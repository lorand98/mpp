
// LAB_EX_1Dlg.h : header file
//

#pragma once


// CLABEX1Dlg dialog
class CLABEX1Dlg : public CDialogEx
{
// Construction
public:
	CLABEX1Dlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LAB_EX_1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	int findMinim();
	int findMaxim();
DECLARE_MESSAGE_MAP()
public:
	int MAX = 5;
	int Counter;
	double dispersie;
	CListBox data;
	double insert;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedOk();
	double minim;
	double maxim;
	double Index;
	double cauta;
	afx_msg void OnBnClickedOk2();
	afx_msg void OnBnClickedOk4();
	bool AreSame(double a, double b);
};
