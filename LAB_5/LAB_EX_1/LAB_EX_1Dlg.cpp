
// LAB_EX_1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "LAB_EX_1.h"
#include "LAB_EX_1Dlg.h"
#include "afxdialogex.h"
#include <string>
#include <vector>
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLABEX1Dlg dialog



CLABEX1Dlg::CLABEX1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LAB_EX_1_DIALOG, pParent)
	, Counter(0)
	, dispersie(0)
	, insert(0)
	, minim(0)
	, maxim(0)
	, Index(0)
	, cauta(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLABEX1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, Counter);
	DDX_Text(pDX, IDC_EDIT3, dispersie);
	DDX_Control(pDX, IDC_LIST1, data);
	DDX_Text(pDX, IDC_EDIT1, insert);
	DDX_Text(pDX, IDC_EDIT5, minim);
	DDX_Text(pDX, IDC_EDIT6, maxim);
	DDX_Text(pDX, IDC_EDIT8, Index);
	DDX_Text(pDX, IDC_EDIT7, cauta);
}

BEGIN_MESSAGE_MAP(CLABEX1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CLABEX1Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDOK, &CLABEX1Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDOK2, &CLABEX1Dlg::OnBnClickedOk2)
	ON_BN_CLICKED(IDOK4, &CLABEX1Dlg::OnBnClickedOk4)
END_MESSAGE_MAP()


// CLABEX1Dlg message handlers

BOOL CLABEX1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLABEX1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLABEX1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLABEX1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

int CLABEX1Dlg::findMinim()
{
	CString str;
	data.GetText(0, str);
	int minim = _ttoi(str);

	for (auto i = 0; i < Counter; i++)
	{
		CString str;
		data.GetText(i, str);

		if(minim > _ttoi(str))
		{
			minim = _ttoi(str);
		}
	}

	return minim;
}

int CLABEX1Dlg::findMaxim()
{
	CString str;
	data.GetText(0, str);
	int maxim = _ttoi(str);

	for (auto i = 0; i < Counter; i++)
	{
		CString str;
		data.GetText(i, str);

		if (maxim < _ttoi(str))
		{
			maxim = _ttoi(str);
		}
	}

	return maxim;
}


void CLABEX1Dlg::OnBnClickedButton1()
{
	if(MAX > Counter)
	{
		UpdateData();

		data.AddString((LPCTSTR)std::to_wstring(insert).c_str());
		Counter = data.GetCount();

		UpdateData(FALSE);
	}
	else
	{
		CString Text;
		Text.Format((LPCWSTR)L"Seria de date are maxim %d de valori!", MAX);
			AfxMessageBox(Text);
	}
	
}

void CLABEX1Dlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	auto suma = 0;

	for (auto i = 0; i < Counter; i++)
	{
		CString str;
		data.GetText(i, str);

		int ceva = _ttoi(str);
		suma += _ttoi(str);
	}

	
	SetDlgItemText(IDC_EDIT4, (CString)((std::string)std::to_string(suma / Counter)).c_str());
	minim = findMinim();
	maxim = findMaxim();

	UpdateData(FALSE);
}


void CLABEX1Dlg::OnBnClickedOk2()
{

	std::vector<double> test;

	// TODO: Add your control notification handler code here
	for (auto i = 0; i < Counter; i++)
	{
		CString str;
		data.GetText(i, str);
		if (std::find(test.begin(), test.end(), _ttoi(str)) == test.end()) 
			test.push_back(_ttoi(str));
	}

	data.ResetContent();

	for( auto i = 0; i <test.size(); i++)
	{
		UpdateData();

		data.AddString((LPCTSTR)std::to_wstring(test[i]).c_str());
		Counter = data.GetCount();

		UpdateData(FALSE);
	}
}



void CLABEX1Dlg::OnBnClickedOk4()
{
	UpdateData();
	for (auto i = 0; i < Counter; i++)
	{
		CString str;
		data.GetText(i, str);

		if(static_cast<int>(cauta) == static_cast<int>(_ttol(str)))
		{
			Index = i;
			break;
		}
	}

	UpdateData(FALSE);

}
