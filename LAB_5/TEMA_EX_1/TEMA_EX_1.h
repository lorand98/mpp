
// TEMA_EX_1.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTEMAEX1App:
// See TEMA_EX_1.cpp for the implementation of this class
//

class CTEMAEX1App : public CWinApp
{
public:
	CTEMAEX1App();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTEMAEX1App theApp;
