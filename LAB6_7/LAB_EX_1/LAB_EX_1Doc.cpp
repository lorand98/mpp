
// LAB_EX_1Doc.cpp : implementation of the CLABEX1Doc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "LAB_EX_1.h"
#endif

#include "LAB_EX_1Doc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CLABEX1Doc

IMPLEMENT_DYNCREATE(CLABEX1Doc, CDocument)

BEGIN_MESSAGE_MAP(CLABEX1Doc, CDocument)
	ON_COMMAND(ID_STELUTE_ADAUGA, &CLABEX1Doc::OnSteluteAdauga)
	ON_COMMAND(ID_STELUTE_STERGE, &CLABEX1Doc::OnSteluteSterge)
END_MESSAGE_MAP()


// CLABEX1Doc construction/destruction

CLABEX1Doc::CLABEX1Doc() noexcept
{
	// TODO: add one-time construction code here
	Stelute = 0;

}

CLABEX1Doc::~CLABEX1Doc()
{
}

BOOL CLABEX1Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CLABEX1Doc serialization

void CLABEX1Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CLABEX1Doc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CLABEX1Doc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data.
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CLABEX1Doc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = nullptr;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != nullptr)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CLABEX1Doc diagnostics

#ifdef _DEBUG
void CLABEX1Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLABEX1Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CLABEX1Doc commands

int CLABEX1Doc::GetStelute(void)
{
	return Stelute;
}





void CLABEX1Doc::OnUpdateSteluteSterge(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(Stelute);
}


void CLABEX1Doc::OnSteluteAdauga()
{
	// TODO: Add your command handler code here

	Stelute++;
	UpdateAllViews(NULL);

}


void CLABEX1Doc::OnSteluteSterge()
{
	// TODO: Add your command handler code here
	if (Stelute > 0)
	{
		Stelute--;
		UpdateAllViews(NULL);
	}
}
