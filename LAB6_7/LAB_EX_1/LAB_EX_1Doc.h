
// LAB_EX_1Doc.h : interface of the CLABEX1Doc class
//


#pragma once


class CLABEX1Doc : public CDocument
{
protected: // create from serialization only
	CLABEX1Doc() noexcept;
	DECLARE_DYNCREATE(CLABEX1Doc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CLABEX1Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
	int GetStelute(void);
	void OnUpdateSteluteSterge(CCmdUI* pCmdUI);
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	int Stelute;

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	afx_msg void OnSteluteAdauga();
	afx_msg void OnSteluteSterge();
};
