
// LAB_EX_1View.cpp : implementation of the CSteluteView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "LAB_EX_1.h"
#endif

#include "LAB_EX_1Doc.h"
#include "LAB_EX_1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSteluteView

IMPLEMENT_DYNCREATE(CSteluteView, CView)

BEGIN_MESSAGE_MAP(CSteluteView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CSteluteView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CSteluteView construction/destruction

CSteluteView::CSteluteView() noexcept
{
	// TODO: add construction code here

}

CSteluteView::~CSteluteView()
{
}

BOOL CSteluteView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CSteluteView drawing

void CSteluteView::OnDraw(CDC* pDC)
{
	if (pDC->IsPrinting()) return;
	CLABEX1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here

	CRect rcClient;
	GetClientRect(&rcClient);
	CBrush brBlue(RGB(0, 0, 255)), *pOldBrush = pDC->SelectObject(&brBlue);
	pDC->Rectangle(rcClient);
	CBrush brYellow(RGB(255, 255, 0));
	pDC->SelectObject(&brYellow);
	pDC->SetPolyFillMode(WINDING);
	const int Raza = rcClient.Width() / 30,
		RazaC = min(rcClient.Width(), rcClient.Height()) / 2 - Raza;
	const double PI = 3.1415;
	const int nColturi = 5;
	const double dUnghi = 2 * PI / (double)nColturi;
	CPoint ptPuncte[nColturi];
	for (int nStea = 0; nStea < pDoc->GetStelute(); nStea++)
	{
		for (int ncolt = 0; ncolt < nColturi; ncolt++)
		{
				ptPuncte[ncolt].x = (long)(cos(2 * (double)ncolt*dUnghi)*Raza);
			ptPuncte[ncolt].y = (long)(sin(2 * (double)ncolt*dUnghi)*Raza);
			ptPuncte[ncolt] += CPoint(rcClient.CenterPoint().x + RazaC *
				cos(2 * PI / pDoc->GetStelute()*nStea), rcClient.CenterPoint().y + RazaC *
				sin(2 * PI / pDoc->GetStelute()*nStea));
		}
		pDC->Polygon(ptPuncte, nColturi);
	}
	pDC->SelectObject(pOldBrush);
}


// CSteluteView printing


void CSteluteView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CSteluteView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSteluteView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSteluteView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CSteluteView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CSteluteView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CSteluteView diagnostics

#ifdef _DEBUG
void CSteluteView::AssertValid() const
{
	CView::AssertValid();
}

void CSteluteView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLABEX1Doc* CSteluteView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLABEX1Doc)));
	return (CLABEX1Doc*)m_pDocument;
}
#endif //_DEBUG


// CSteluteView message handlers

void CSteluteView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	//TODO: Add your specialized code here and/or call the base class
	CLABEX1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	//TODO: add draw code for native data here
	CRect rcClient = pInfo->m_rectDraw;
	pDC->SelectStockObject(BLACK_BRUSH);
	pDC->SetPolyFillMode(WINDING);
	const int Raza = rcClient.Width() / 30,
		RazaC = min(rcClient.Width(), rcClient.Height()) / 2 - Raza;
	const double PI = 3.1415;
	const int nColturi = 5;
	const double dUnghi = 2 * PI / (double)nColturi;
	CPoint ptPuncte[nColturi];
	for (int nStea = 0; nStea < pDoc->GetStelute(); nStea++)
	{
		for (int ncolt = 0; ncolt < nColturi; ncolt++)

		{
			ptPuncte[ncolt].x = (long)(sin(2 * (double)ncolt*dUnghi)*Raza);
			ptPuncte[ncolt].y = (long)(cos(2 * (double)ncolt*dUnghi)*Raza);
			ptPuncte[ncolt] += CPoint(rcClient.CenterPoint().x + RazaC * sin(2 * PI /
				pDoc->GetStelute()*nStea),
				rcClient.CenterPoint().y + RazaC * cos(2 * PI / pDoc->GetStelute()*nStea));
		}
		pDC->Polygon(ptPuncte, nColturi);
	}
	CSteluteView::OnPrint(pDC, pInfo);
}
