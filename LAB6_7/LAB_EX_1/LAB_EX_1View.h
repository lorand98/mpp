
// LAB_EX_1View.h : interface of the CSteluteView class
//

#pragma once


class CSteluteView : public CView
{
protected: // create from serialization only
	CSteluteView() noexcept;
	DECLARE_DYNCREATE(CSteluteView)

// Attributes
public:
	CLABEX1Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual void OnPrint(CDC * pDC, CPrintInfo * pInfo);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CSteluteView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LAB_EX_1View.cpp
inline CLABEX1Doc* CSteluteView::GetDocument() const
   { return reinterpret_cast<CLABEX1Doc*>(m_pDocument); }
#endif

