
// Estimation ToolDlg.h : header file
//

#pragma once
#include <xstring>


// CEstimationToolDlg dialog
class CEstimationToolDlg : public CDialogEx
{
// Construction
public:
	CEstimationToolDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ESTIMATIONTOOL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double Analysys;
	double Planning;
	double Review;
	double Retrospective;
	double Overtime;
	double SprintDuration;
	double WorkingDays;
	int HoursDay;
	CSliderCtrl HoursDaySliderControl;
	CSliderCtrl ControlSliderDay;
	CDateTimeCtrl SetupDateStart;
	CDateTimeCtrl SetupDateEnd;
	CDateTimeCtrl VacationDate;
	CDateTimeCtrl PeriodCapacityStart;
	CDateTimeCtrl PeriodCapacityEnd;
	CListCtrl FinalList;
	CListCtrl VacationList;
	double daily;
	afx_msg void on_nm_custom_draw_hour_slider(NMHDR *pNMHDR, LRESULT *pResult);
	void setLogTable();
	void getCapacity();
	int DailyHoursShow;
	double Capacity;
	CString User;
	int NonWorkDays;
	afx_msg void on_bn_clicked_add_button();
	static std::string get_day_name(CTime time);
	std::string get_day_name_pointer(CTime& time);
	std::string get_format_date(CTime time) const;
	std::string get_format_date_pointer(CTime& time) const;
	afx_msg void on_bn_clicked_button3();
	double RepetableHour;
	afx_msg void on_bn_clicked_delete_button();
	void delete_selected_item(CListCtrl& list);
	afx_msg void OnBnClickedButton4();
	afx_msg void OnDtnDatetimechangeStartDate(NMHDR *pNMHDR, LRESULT *pResult);
	void setFinalTableLog();
	bool insertRepetableDay(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date);
	bool insertVacationItem(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date);
	void insertTableItem(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date, std::string description);
	void setCapacityDays();
	void setWorkingDays();
	afx_msg void OnDtnDatetimechangeEndDate(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeAnalysis();
	afx_msg void OnEnChangePlaning();
	afx_msg void OnEnChangeReview();
	afx_msg void OnEnChangeRetrospective();
	afx_msg void OnEnChangeOvertime();
	int TotalCapacity;
	CListCtrl LogTable;
	CComboBox RepetableWorkPicker;
	CListCtrl RepetableWork;
	CString RepetableDay;
	afx_msg void OnBnClickedOk2();
	std::string getStringFromCString(CString str);
};
