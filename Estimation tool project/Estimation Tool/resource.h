//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EstimationTool.rc
//
#define IDOK2                           3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ESTIMATIONTOOL_DIALOG       102
#define IDR_MAINFRAME                   128
#define SETUP_START_DATE                1000
#define SETUP_END_DATE                  1001
#define SETUP_ANALYSIS                  1002
#define SETUP_WORKING_DAYS              1003
#define SETUP_SPRINT_WORKING            1004
#define SETUP_SPRINT_DURATION           1005
#define SETUP_PLANING                   1006
#define SETUP_REVIEW                    1007
#define SETUP_RETROSPECTIVE             1008
#define SETUP_OVERTIME                  1009
#define SETUP_DAILY_MEETING             1010
#define SETUP_HOURS                     1011
#define SETUP_HOUR_SLIDER               1012
#define VACATION_DATE                   1013
#define VACATION_ADD_BUTTON             1014
#define VACATION_DELETE_BUTTON          1015
#define IDC_DATETIMEPICKER4             1017
#define IDC_BUTTON3                     1018
#define IDC_BUTTON4                     1019
#define IDC_EDIT12                      1022
#define IDC_DATETIMEPICKER5             1024
#define IDC_DATETIMEPICKER6             1025
#define IDC_EDIT13                      1026
#define IDC_EDIT14                      1027
#define IDC_EDIT15                      1028
#define IDC_EDIT16                      1029
#define FINAL_TABLE                     1030
#define VACATION_TABLE                  1031
#define IDC_LIST6                       1032
#define IDC_EDIT17                      1033
#define IDC_COMBO1                      1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
