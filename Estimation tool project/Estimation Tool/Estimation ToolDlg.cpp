
// Estimation ToolDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Estimation Tool.h"
#include "Estimation ToolDlg.h"
#include "afxdialogex.h"
#include <windows.h>
#include <Lmcons.h>
#include <string>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CEstimationToolDlg dialog



CEstimationToolDlg::CEstimationToolDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ESTIMATIONTOOL_DIALOG, pParent)
	, Analysys(0)
	, Planning(0)
	, Review(0)
	, Retrospective(0)
	, Overtime(0)
	, SprintDuration(0)
	, WorkingDays(0)
	, HoursDay(8)
	, daily(4)
	, DailyHoursShow(0)
	, Capacity(0)
	, User(_T(""))
	, NonWorkDays(0)
	, RepetableHour(0)
	, TotalCapacity(0)
	, RepetableDay(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	
}

void CEstimationToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, SETUP_ANALYSIS, Analysys);
	DDX_Text(pDX, SETUP_PLANING, Planning);
	DDX_Text(pDX, SETUP_REVIEW, Review);
	DDX_Text(pDX, SETUP_RETROSPECTIVE, Retrospective);
	DDX_Text(pDX, SETUP_OVERTIME, Overtime);
	DDX_Text(pDX, SETUP_SPRINT_DURATION, SprintDuration);
	DDX_Text(pDX, SETUP_WORKING_DAYS, WorkingDays);
	DDX_Slider(pDX, SETUP_HOUR_SLIDER, HoursDay);
	DDX_Control(pDX, SETUP_HOUR_SLIDER, ControlSliderDay);
	DDX_Control(pDX, SETUP_START_DATE, SetupDateStart);
	DDX_Control(pDX, SETUP_END_DATE, SetupDateEnd);
	DDX_Control(pDX, VACATION_DATE, VacationDate);
	DDX_Control(pDX, VACATION_TABLE, VacationList);
	DDX_Text(pDX, SETUP_DAILY_MEETING, daily);
	DDX_Text(pDX, SETUP_HOURS, DailyHoursShow);
	DDX_Text(pDX, IDC_EDIT16, Capacity);
	DDX_Text(pDX, IDC_EDIT14, User);
	DDX_Text(pDX, IDC_EDIT15, NonWorkDays);
	DDX_Text(pDX, IDC_EDIT12, RepetableHour);
	DDX_Text(pDX, IDC_EDIT17, TotalCapacity);
	DDX_Control(pDX, FINAL_TABLE, LogTable);
	DDX_Control(pDX, IDC_COMBO1, RepetableWorkPicker);
	DDX_Control(pDX, IDC_LIST6, RepetableWork);
	DDX_CBString(pDX, IDC_COMBO1, RepetableDay);
}

BEGIN_MESSAGE_MAP(CEstimationToolDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(NM_CUSTOMDRAW, SETUP_HOUR_SLIDER, &CEstimationToolDlg::on_nm_custom_draw_hour_slider)
	ON_BN_CLICKED(VACATION_ADD_BUTTON, &CEstimationToolDlg::on_bn_clicked_add_button)
	ON_BN_CLICKED(IDC_BUTTON3, &CEstimationToolDlg::on_bn_clicked_button3)
	ON_BN_CLICKED(VACATION_DELETE_BUTTON, &CEstimationToolDlg::on_bn_clicked_delete_button)
	ON_BN_CLICKED(IDC_BUTTON4, &CEstimationToolDlg::OnBnClickedButton4)
	ON_NOTIFY(DTN_DATETIMECHANGE, SETUP_START_DATE, &CEstimationToolDlg::OnDtnDatetimechangeStartDate)
	ON_NOTIFY(DTN_DATETIMECHANGE, SETUP_END_DATE, &CEstimationToolDlg::OnDtnDatetimechangeEndDate)
	ON_EN_CHANGE(SETUP_ANALYSIS, &CEstimationToolDlg::OnEnChangeAnalysis)
	ON_EN_CHANGE(SETUP_PLANING, &CEstimationToolDlg::OnEnChangePlaning)
	ON_EN_CHANGE(SETUP_REVIEW, &CEstimationToolDlg::OnEnChangeReview)
	ON_EN_CHANGE(SETUP_RETROSPECTIVE, &CEstimationToolDlg::OnEnChangeRetrospective)
	ON_EN_CHANGE(SETUP_OVERTIME, &CEstimationToolDlg::OnEnChangeOvertime)
	ON_BN_CLICKED(IDOK2, &CEstimationToolDlg::OnBnClickedOk2)
END_MESSAGE_MAP()


// CEstimationToolDlg message handlers

BOOL CEstimationToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	ControlSliderDay.SetRange(1, 8, TRUE);

	LogTable.InsertColumn(0, _T("Date"), LVCFMT_LEFT, 100);
	LogTable.InsertColumn(1, _T("Day"), LVCFMT_LEFT, 150);
	LogTable.InsertColumn(2, _T("Description"), LVCFMT_LEFT, 300);

	VacationList.InsertColumn(0, _T("Date"), LVCFMT_LEFT, 50);
	VacationList.InsertColumn(1, _T("Day"), LVCFMT_LEFT, 100);

	RepetableWork.InsertColumn(1, _T("Day"), LVCFMT_LEFT, 50);
	RepetableWork.InsertColumn(2, _T("Hours"), LVCFMT_LEFT, 100);

	RepetableWorkPicker.InsertString(0,(LPCTSTR)L"Monday");
	RepetableWorkPicker.InsertString(1, (LPCTSTR)L"Tuesday");
	RepetableWorkPicker.InsertString(2, (LPCTSTR)L"Wednesday");
	RepetableWorkPicker.InsertString(3, (LPCTSTR)L"Thursday");
	RepetableWorkPicker.InsertString(4, (LPCTSTR)L"Friday");


	TCHAR username[UNLEN + 1];
	DWORD size = UNLEN + 1;
	GetUserName((TCHAR*)username, &size);

	User = username;

	//int nIndex = FinalList.InsertItem(0, _T("Sandra C. Anschwitz"));
	//FinalList.SetItemText(nIndex, 1, _T("Singer"));
	//FinalList.SetItemText(nIndex, 2, _T("Handball"));
	//FinalList.SetItemText(nIndex, 3, _T("Beach"));

	//nIndex = FinalList.InsertItem(1, _T("Roger A. Miller"));
	//FinalList.SetItemText(nIndex, 1, _T("Footballer"));
	//FinalList.SetItemText(nIndex, 2, _T("Tennis"));
	//FinalList.SetItemText(nIndex, 3, _T("Teaching"));
	UpdateData(false);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEstimationToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEstimationToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEstimationToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CEstimationToolDlg::on_nm_custom_draw_hour_slider(NMHDR *pNMHDR, LRESULT *pResult)
{
	UpdateData();
	DailyHoursShow = HoursDay;
	UpdateData(false);

	getCapacity();
}

void CEstimationToolDlg::setLogTable()
{
	UpdateData();



	UpdateData(false);
}

void CEstimationToolDlg::getCapacity()
{
	UpdateData();

	auto final_capacity = -DailyHoursShow * VacationList.GetItemCount() + WorkingDays * 
		DailyHoursShow - Analysys - Planning - Review - Retrospective + Overtime;

	for(auto i = 0; i < RepetableWork.GetItemCount(); i++)
	{
		auto item = RepetableWork.GetItemText(i, 2);
		const auto hours = _ttoi(item);
		final_capacity -= hours;
	}

	Capacity = final_capacity;
	TotalCapacity = DailyHoursShow * WorkingDays;

	UpdateData(false);
}

std::string CEstimationToolDlg::get_day_name(CTime time)
{
	return time.GetDayOfWeek() == 1 ? "Monday" :
		time.GetDayOfWeek() == 2 ? "Tuesday" :
		time.GetDayOfWeek() == 3 ? "Wednesday" :
		time.GetDayOfWeek() == 4 ? "Thursday" :
		time.GetDayOfWeek() == 5 ? "Friday" :
		time.GetDayOfWeek() == 6 ? "Saturday" : "Sunday";
}

std::string CEstimationToolDlg::get_format_date(CTime time) const
{
	return std::to_string(time.GetDay()) + "/" +
		std::to_string(time.GetMonth()) + "/" +
		std::to_string(time.GetYear());
}

void CEstimationToolDlg::on_bn_clicked_add_button()
{
	// TODO: Add your control notification handler code here
	UpdateData();

	CTime vacation_time;
	// ReSharper disable once CppExpressionWithoutSideEffects
	VacationDate.GetTime(vacation_time);

	auto day = get_day_name(vacation_time);
	auto date = get_format_date(vacation_time);

	auto check = true;
	for(auto i = 0; i < VacationList.GetItemCount(); i++)
	{
		auto item = VacationList.GetItemText(i, 1);
		CT2CA psz_converted_ansi_string(item);
		std::string s(psz_converted_ansi_string);
		if (date == s){
			check = false;
		};
	}

	if(check)
	{
		const auto n_index = VacationList.InsertItem(0, static_cast<LPCTSTR>(static_cast<CString>(day.c_str())));
		VacationList.SetItemText(n_index, 1, static_cast<LPCTSTR>(static_cast<CString>(date.c_str())));
	}

	setFinalTableLog();
	NonWorkDays = VacationList.GetItemCount();
	UpdateData(false);
	
	getCapacity();
}

void CEstimationToolDlg::on_bn_clicked_button3()
{
	UpdateData();

	CTime repeatable_time;
	// ReSharper disable once CppExpressionWithoutSideEffects

	const auto n_index = RepetableWork.InsertItem(0, static_cast<LPCTSTR>(RepetableDay));
	RepetableWork.SetItemText(n_index, 1, 
		static_cast<LPCTSTR>(static_cast<CString>(std::to_string(RepetableHour).c_str())));

	UpdateData(false);
	getCapacity();
	setFinalTableLog();
}


void CEstimationToolDlg::on_bn_clicked_delete_button()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	delete_selected_item(VacationList);
	NonWorkDays = VacationList.GetItemCount();
	UpdateData(false);
	getCapacity();
	setFinalTableLog();

}

void CEstimationToolDlg::delete_selected_item(CListCtrl& list)
{
	const auto u_selected_count = list.GetSelectedCount();
	if (u_selected_count > 0)
		for (UINT i = 0; i < u_selected_count; i++)
		{
			const auto n_item = list.GetNextItem(-1, LVNI_SELECTED);
			ASSERT(n_item != -1);
			list.DeleteItem(n_item);
		}

}

void CEstimationToolDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	delete_selected_item(RepetableWork);
	UpdateData(false);
	setFinalTableLog();
	getCapacity();
}


void CEstimationToolDlg::OnDtnDatetimechangeStartDate(NMHDR *pNMHDR, LRESULT *pResult)
{
	setCapacityDays();
	getCapacity();
}

void CEstimationToolDlg::setFinalTableLog()
{
	UpdateData();

	CTime startTime, endTime;
	SetupDateStart.GetTime(startTime);
	SetupDateEnd.GetTime(endTime);

	LogTable.SetRedraw(FALSE);
	LogTable.DeleteAllItems();
	LogTable.SetRedraw(TRUE);

	auto tempt = new CTime(startTime.GetTime());
	while (true)
	{
		if (tempt->GetTime() > endTime.GetTime()) break;

		if (!(tempt->GetDayOfWeek() == 7 || tempt->GetDayOfWeek() == 6)) {
			auto const day = tempt->GetDayOfWeek() == 1 ? "Monday" :
				tempt->GetDayOfWeek() == 2 ? "Tuesday" :
				tempt->GetDayOfWeek() == 3 ? "Wednesday" :
				tempt->GetDayOfWeek() == 4 ? "Thursday" :
				tempt->GetDayOfWeek() == 5 ? "Friday" :
				tempt->GetDayOfWeek() == 6 ? "Saturday" : "Sunday";

			auto const date = std::to_string(tempt->GetDay()) + "/" +
				std::to_string(tempt->GetMonth()) + "/" +
				std::to_string(tempt->GetYear());

			if (insertVacationItem(day, date)) {
				tempt = new CTime(tempt->GetTime() + 86400);
				continue;
			}

			if(insertRepetableDay(day, date))
			{
				tempt = new CTime(tempt->GetTime() + 86400);
				continue;
			}

			insertTableItem(day, date, "Working day");
		}

		tempt = new CTime(tempt->GetTime() + 86400);

	}

	UpdateData(false);
}

bool CEstimationToolDlg::insertRepetableDay(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date)
{
	for (auto i = 0; i < RepetableWork.GetItemCount(); i++)
	{
		auto item = RepetableWork.GetItemText(i, 0);
		CT2CA psz_converted_ansi_string(item);
		std::string s(psz_converted_ansi_string);
		if(static_cast<std::string>(day) == s)
		{
			insertTableItem(day, date, "Repetable Work");
			return true;
		}
	}

	return false;
}

bool CEstimationToolDlg::insertVacationItem(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date)
{
	for (auto i = 0; i < VacationList.GetItemCount(); i++)
	{
		auto item = VacationList.GetItemText(i, 1);
		CT2CA psz_converted_ansi_string(item);
		std::string s(psz_converted_ansi_string);
		if ((std::string)date.c_str() == s) {
			insertTableItem(day, date, "Vacation Day");
			return true;
		};
	}

	return false;
	
}

void CEstimationToolDlg::insertTableItem(const char* day, std::basic_string<char, std::char_traits<char>, std::allocator<char>> date,std::string description)
{
	const auto n_index = LogTable.InsertItem(0, static_cast<LPCTSTR>(static_cast<CString>(day)));
	LogTable.SetItemText(n_index, 1, static_cast<LPCTSTR>(static_cast<CString>(date.c_str())));
	LogTable.SetItemText(n_index, 2, static_cast<LPCTSTR>(static_cast<CString>(description.c_str())));
}

void CEstimationToolDlg::setCapacityDays()
{
	UpdateData();

	CTime startTime, endTime;
	SetupDateStart.GetTime(startTime);
	SetupDateEnd.GetTime(endTime);

	const auto finaltime = new CTime(endTime.GetTime() - startTime.GetTime());
	const int nr_of_days = finaltime->GetTime() / 86400;

	SprintDuration = nr_of_days;

	auto tempt = new CTime(startTime.GetTime());
	auto counterDays = 0;
	while(true)
	{
		if (tempt->GetTime() > endTime.GetTime()) break;

		if (tempt->GetDayOfWeek() == 7 || tempt->GetDayOfWeek() == 6) counterDays++;
		tempt = new CTime(tempt->GetTime() + 86400);
	}

	WorkingDays = nr_of_days - counterDays;

	UpdateData(false);

	setFinalTableLog();
}

void CEstimationToolDlg::OnDtnDatetimechangeEndDate(NMHDR *pNMHDR, LRESULT *pResult)
{
	setCapacityDays();
	getCapacity();
}


void CEstimationToolDlg::OnEnChangeAnalysis()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	getCapacity();
}


void CEstimationToolDlg::OnEnChangePlaning()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	getCapacity();
}


void CEstimationToolDlg::OnEnChangeReview()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	getCapacity();
}


void CEstimationToolDlg::OnEnChangeRetrospective()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	getCapacity();
}


void CEstimationToolDlg::OnEnChangeOvertime()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	getCapacity();
}


void CEstimationToolDlg::OnBnClickedOk2()
{
	// TODO: Add your control notification handler code here
	std::ofstream outfile("Capacity_" + getStringFromCString(User) + ".txt");

	outfile << "User: " << getStringFromCString(User) << std::endl;
	outfile << "Capacity: " <<  std::to_string(Capacity) << std::endl;
	outfile << "Total Capacity: " << std::to_string(TotalCapacity) << std::endl;
	outfile << std::endl;
	outfile << "Capacity Log:" << std::endl;

	for(auto i = 0; i < LogTable.GetItemCount(); i++)
	{
		std::string row;

		row = "Day: " + getStringFromCString(LogTable.GetItemText(i, 0)) + "\tDate: " +
			getStringFromCString(LogTable.GetItemText(i, 1)) + "\tDescription: " +
			getStringFromCString(LogTable.GetItemText(i, 2));

		outfile << row << std::endl;
	}

	outfile.close();

}

std::string CEstimationToolDlg::getStringFromCString(CString str)
{
	CT2CA pszConvertedAnsiString(str);
	std::string ceva(pszConvertedAnsiString);
	return ceva;
}
