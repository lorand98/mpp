
// TEMA_EX1Dlg.h : header file
//

#pragma once
#include <vector>


// CTEMAEX1Dlg dialog
class CTEMAEX1Dlg : public CDialogEx
{
// Construction
public:
	CTEMAEX1Dlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEMA_EX1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	
DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCalculeaza();
	std::vector<int> getArray(CString text);
	int getCMMMCFromArray(std::vector<int> values);
	int getCMMDCFromArray(std::vector<int> values);
};
