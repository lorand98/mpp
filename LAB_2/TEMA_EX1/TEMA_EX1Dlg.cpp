
// TEMA_EX1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "TEMA_EX1.h"
#include "TEMA_EX1Dlg.h"
#include "afxdialogex.h"
#include <string>
#include <sstream>
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTEMAEX1Dlg dialog



CTEMAEX1Dlg::CTEMAEX1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TEMA_EX1_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTEMAEX1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTEMAEX1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(Calculeaza, &CTEMAEX1Dlg::OnBnClickedCalculeaza)
END_MESSAGE_MAP()


// CTEMAEX1Dlg message handlers

BOOL CTEMAEX1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTEMAEX1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTEMAEX1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTEMAEX1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

std::vector<int> CTEMAEX1Dlg::getArray(CString text)
{
	GetDlgItemText(A_NR, text);
	std::string STDStr(CW2A(text.GetString(), CP_UTF8));

	std::stringstream stream(STDStr);
	std::vector<int> values;
	int n;

	while (stream >> n) {
		values.push_back(n);
	}

	return values;
}

int CTEMAEX1Dlg::getCMMMCFromArray(std::vector<int> values)
{
	int cmmmc = values[0];
	for (int i = 1; i < values.size(); i++) {
		int copieCMMMC = cmmmc, copieEl = values[i];

		while (copieCMMMC != copieEl)
		{
			if (copieCMMMC > copieEl)
				copieCMMMC -= copieEl;
			else
				copieEl -= copieCMMMC;
		}

		cmmmc = cmmmc * (values[i] / copieEl);
	}

	return cmmmc;
}

int CTEMAEX1Dlg::getCMMDCFromArray(std::vector<int> values)
{
	int cmmdc = values[0];

	for (int i = 1; i < values.size(); i++)
	{
		int copieEL = values[i];

		while(copieEL != cmmdc)
		{
			if (copieEL > cmmdc)
				copieEL -= cmmdc;
			else
				cmmdc -= copieEL;
		}
	}

	return cmmdc;
}


void CTEMAEX1Dlg::OnBnClickedCalculeaza()
{
	CString text;
	std::vector<int> values = getArray(text);

	if(values.size() > 0)
	{
		SetDlgItemText(CMMMC, (CString)((std::string)std::to_string(getCMMMCFromArray(values))).c_str());
		SetDlgItemText(CMMDC, (CString)((std::string)std::to_string(getCMMDCFromArray(values))).c_str());
	}
	else
	{
		AfxMessageBox((LPCWSTR)L"Nu ai scris nici un numar");
	}
}
