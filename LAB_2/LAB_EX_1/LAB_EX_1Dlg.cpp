
// LAB_EX_1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "LAB_EX_1.h"
#include "LAB_EX_1Dlg.h"
#include "afxdialogex.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLABEX1Dlg dialog



CLABEX1Dlg::CLABEX1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LAB_EX_1_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLABEX1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLABEX1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(Calculeaza, &CLABEX1Dlg::OnBnClickedCalculeaza)
	ON_BN_CLICKED(Sterge, &CLABEX1Dlg::OnBnClickedSterge)
END_MESSAGE_MAP()


// CLABEX1Dlg message handlers

BOOL CLABEX1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLABEX1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLABEX1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLABEX1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

int CLABEX1Dlg::getCMMMC(int a, int b)
{
	int x = a, y = b;
	while (x != y)
	{
		if (x > y)
			x -= y;
		else
			y -= x;
	}
	return (a*b) / x;
}

int CLABEX1Dlg::getCMMDC(int a, int b)
{
	while (a != b)
	{
		if (a > b)
			a -= b;
		else
			b -= a;
	}
	return a;
}

void CLABEX1Dlg::OnBnClickedCalculeaza()
{
	CString textA,textB;
	GetDlgItemText(A_NR, textA);
	GetDlgItemText(B_NR, textB);
	int a = _ttoi(textA), b = _ttoi(textB);

	if(a != NULL && b != NULL )
	{	
		SetDlgItemText(CMMMC, (CString)((std::string)std::to_string(getCMMMC(a,b))).c_str());	
		SetDlgItemText(CMMDC, (CString)((std::string)std::to_string(getCMMDC(a,b))).c_str());
	}
}


void CLABEX1Dlg::OnBnClickedSterge()
{
	SetDlgItemText(A_NR, (CString)((std::string)"").c_str());
	SetDlgItemText(B_NR, (CString)((std::string)"").c_str());
	SetDlgItemText(CMMMC, (CString)((std::string)"").c_str());
	SetDlgItemText(CMMDC, (CString)((std::string)"").c_str());
}
