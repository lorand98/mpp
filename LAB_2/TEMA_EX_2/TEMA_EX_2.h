
// TEMA_EX_2.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CTEMAEX2App:
// See TEMA_EX_2.cpp for the implementation of this class
//

class CTEMAEX2App : public CWinApp
{
public:
	CTEMAEX2App();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CTEMAEX2App theApp;
