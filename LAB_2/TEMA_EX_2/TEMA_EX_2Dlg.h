
// TEMA_EX_2Dlg.h : header file
//

#pragma once


// CTEMAEX2Dlg dialog
class CTEMAEX2Dlg : public CDialogEx
{
// Construction
public:
	CTEMAEX2Dlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEMA_EX_2_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	int GCD(int A, int B);
DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCalculeaza();
};
