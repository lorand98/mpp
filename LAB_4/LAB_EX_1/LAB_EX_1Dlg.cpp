
// LAB_EX_1Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "LAB_EX_1.h"
#include "LAB_EX_1Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLABEX1Dlg dialog



CLABEX1Dlg::CLABEX1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LAB_EX_1_DIALOG, pParent)
	, Iesire(0)
	, Intrare(0)
	, reducere(FALSE)
	, valoare(_T(""))
	, Counter(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	list["Arizona Dream"] = 10;
	list["Matrix"] = 75;
	list["Al saselea simt"] = 134;
	list["Al cincilea element"] = 145;
	list["Winx"] = 123;
	list["H2O"] = 13;
	list["Spiderman"] = 150;
	list["Mumia"] = 23;



}

void CLABEX1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, Lista);
	DDX_Text(pDX, IDC_EDIT2, Iesire);
	DDX_Text(pDX, IDC_EDIT1, Intrare);
	DDX_Check(pDX, IDC_CHECK1, reducere);
	DDX_CBString(pDX, IDC_COMBO1, valoare);
	DDX_Control(pDX, IDC_COMBO1, zile);
	DDX_Text(pDX, COUNTER, Counter);
}

BEGIN_MESSAGE_MAP(CLABEX1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CLABEX1Dlg::OnBnClickedOk)
	ON_LBN_SELCHANGE(IDC_LIST, &CLABEX1Dlg::OnLbnSelchangeList)
END_MESSAGE_MAP()


// CLABEX1Dlg message handlers

BOOL CLABEX1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	Lista.AddString((LPCWSTR)L"Arizona Dream");
	Lista.AddString((LPCWSTR)L"Matrix");
	Lista.AddString((LPCWSTR)L"Al saselea simt");
	Lista.AddString((LPCWSTR)L"Al cincilea element");
	Lista.AddString((LPCWSTR)L"Winx");
	Lista.AddString((LPCWSTR)L"H2O");
	Lista.AddString((LPCWSTR)L"Spiderman");
	Lista.AddString((LPCWSTR)L"Mumia");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLABEX1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLABEX1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLABEX1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CLABEX1Dlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	double sum = 0;
	Iesire = (zile.GetCurSel() + 1)*Intrare*Lista.GetSelCount()*(reducere ? 0.75 : 1);

	auto *lp = new int[list.size()];
	const auto count2 = Lista.GetSelItems(list.size(), lp);

	for (auto i = 0; i < count2; i++)
	{
		CString str;
		Lista.GetText(lp[i], str);
		auto key = static_cast<std::string>(CW2A(str.GetString(), CP_UTF8));
		
		if(list.find(key) != list.end())
		{
			sum += (zile.GetCurSel() + 1)*(list[key] + Intrare) * (reducere ? 0.75 : 1);
		}
	}

	Iesire = sum;

	UpdateData(FALSE);
}


void CLABEX1Dlg::OnLbnSelchangeList()
{
	UpdateData();
	Counter = Lista.GetSelCount();
	UpdateData(FALSE);
}
