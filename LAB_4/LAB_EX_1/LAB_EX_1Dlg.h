
// LAB_EX_1Dlg.h : header file
//

#pragma once
#include <string>
#include <map>


// CLABEX1Dlg dialog
class CLABEX1Dlg : public CDialogEx
{
// Construction
public:
	CLABEX1Dlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LAB_EX_1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CListBox Lista;
	double Iesire;
	double Intrare;
	BOOL reducere;
	CString valoare;
	CComboBox zile;
	std::map<std::string, int> list;
	int Counter;
	afx_msg void OnLbnSelchangeList();
};
